//Chris Hinson
// Chapter 8 Program 1

public class Area
{
   //area method for circle
   public static double shapeArea(double radius)
   {
      return (radius * radius * Math.PI);
   }
   //area method for rectangle
   public static double shapeArea(double w, double l)
   {
      return w * l;
   }
   //area method for cylinder
   public static double shapeArea(float rad, double h)
   {
      return rad * rad * Math.PI * h;
   }
}
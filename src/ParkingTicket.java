import static java.lang.Math.ceil;

public class ParkingTicket
{
    private PoliceOfficer officer;
    private ParkedCar car;

    private double fine;



    public ParkingTicket (PoliceOfficer officer, ParkedCar car)
    {
        this.officer = officer;
        this.car = car;

        this.fine = getFine();
    }

    public double getFine()
    {
        if ((car.getMinutesParked()-car.getMeter().getNumOfMinutesPurchased())<=60)
        {
            return 25.00;
        }
        else
        {
            double fine = 25.00;
            double minutesMinusFirstHour = ((car.getMinutesParked()-car.getMeter().getNumOfMinutesPurchased())-60);

            double extraFine = ceil((minutesMinusFirstHour/60))*10.00;

            fine = fine+extraFine;

            return fine;
        }
    }


}

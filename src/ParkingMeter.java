public class ParkingMeter
{
    private int numOfMinutesPurchased;

    public ParkingMeter(int numOfMinutesPurchased)
    {
        this.numOfMinutesPurchased = numOfMinutesPurchased;
    }

    public int getNumOfMinutesPurchased() {
        return numOfMinutesPurchased;
    }
}

public class ParkedCar
{
    private String make, model, color, license;
    private int minutesParked;

    private ParkingMeter meter;

    private ParkingTicket ticket;

    public ParkedCar(String make, String model, String color, String license, int minutesParked, ParkingMeter meter)
    {
        this.make = make;
        this.model = model;
        this.color = color;
        this.license = license;
        this.minutesParked = minutesParked;

        this.meter = meter;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public String getLicense() {
        return license;
    }

    public int getMinutesParked() {
        return minutesParked;
    }

    public ParkingMeter getMeter() {
        return meter;
    }


    public void setMinutesParked(int minutesParked) {
        this.minutesParked = minutesParked;
    }


    public StringBuilder objectToString()
    {
        StringBuilder string = new StringBuilder(
                "You have the " + make + " " + model +
                "\nColor: " + color +
                "\nLicense Plate: " + license +
                "\nYou have been parked for " + minutesParked + " on "+ meter.getNumOfMinutesPurchased() + " minutes purchased");

        if (ticket!=null)
        {
            string.append("\nYou have a ticket for " + ticket.getFine());
        }

        return string;
    }


    public void giveTicket(ParkingTicket ticket)
    {
        this.ticket = ticket;
    }

    public void checkCar()
    {
        System.out.println(this.objectToString());
    }

}

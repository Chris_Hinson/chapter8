import java.util.Scanner;

public class prg8
{

    public static void main(String args[])
    {
        Scanner k = new Scanner(System.in);

        ParkedCar car;
        String make,model,color,license;
        int minutesPurchsed;


        System.out.println("Please enter make of car");
        make = k.next();
        System.out.println("Please enter model of car");
        model = k.next();
        System.out.println("Please enter color of car");
        color = k.next();
        System.out.println("Please enter license of car");
        license = k.next();


        System.out.println("How many minutes would you like to purchase?");
        minutesPurchsed = k.nextInt();


        ParkingMeter meter = new ParkingMeter(minutesPurchsed);

        car = new ParkedCar(make,model, color, license, 0, meter);


        System.out.println("How many minutes have passed?");
        int minutesPassed = k.nextInt();
        car.setMinutesParked(minutesPassed);


        PoliceOfficer officer = new PoliceOfficer("Sergeant Terry Jeffords", 304885);
        officer.inspect(car);

        car.checkCar();

    }

}

//Chris Hinson
//Chapter 8 Program 5

public class Month
{
   private int monthNumber;
   private String name;
  
   public void Month()
   {
      monthNumber = 1;
   }
   public void Month(int m)
   {
      m = monthNumber;
      
      if (m > 12)
      {
         monthNumber = 1;
      }
      if (m < 1)
      {
         monthNumber = 1;
      }
   }
   public void monthStr(String month)
   {
      switch(month)
      {
       case "January":
       name = "January";
       monthNumber = 1;
       break;
       case "February":
       name = "February";
       monthNumber = 2;
       break;
       case "March":
       name = "March";
       monthNumber = 3;
       break;
       case "April":
       name = "April";
       monthNumber = 4;
       break;
       case "May":
       name = "May";
       monthNumber = 5;
       break;
       case "June":
       name = "June";
       monthNumber = 6;
       break;
       case "July":
       name = "July";
       monthNumber = 7;
       break;
       case "August":
       name = "August";
       monthNumber = 8;
       break;
       case "September":
       name = "September";
       monthNumber = 9;
       break;
       case "October":
       name = "October";
       monthNumber = 10;
       break;
       case "November":
       name = "November";
       monthNumber = 11;
       break;
       case "December":
       name = "December";
       monthNumber = 12;
       break;
       default:
       {
         System.out.println("Not a valid month");
         name = "January";
         monthNumber = 1;
       }
      }
   }
      public void setMonthNumber(int mon)
      {
      mon = monthNumber;
      
         if (mon < 1)
         {
            monthNumber = 1;
         }
         if (mon > 12)
         {
            monthNumber = 1;
         }
      }
      public int getMonthNumber()
      {
         return monthNumber;
      }
      public String getMonthName()
      {
         return name;
      }
      public String toString()
      {
         String n = name;
         return n;
      }
      public boolean equals(Month obj)
      {
         boolean check;
         if(monthNumber == obj.monthNumber)
         {
            check = true;
         }
         else
         {
            check = false;
         }
         return check;
      }
      public boolean greaterThan(Month obj)
      {
         boolean check;
         if(monthNumber > obj.monthNumber)
         {
            check = true;
         }
         else
         {
            check = false;
         }
         return check;
      }
      public boolean lessThan(Month obj)
      {
         boolean check;
         if(monthNumber < obj.monthNumber)
         {
            check = true;
         }
         else
         {
            check = false;
         }
         return check;
      }
      
      
   } 
      
   
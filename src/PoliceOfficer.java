public class PoliceOfficer
{
    private String name;
    private int badgeNumber;

    public PoliceOfficer(String name, int badgeNumber)
    {
        this.name = name;
        this.badgeNumber = badgeNumber;
    }

    public String getName() {
        return name;
    }

    public int getBadgeNumber() {
        return badgeNumber;
    }

    public Boolean inspect(ParkedCar car)
    {
        if (car.getMeter().getNumOfMinutesPurchased()<car.getMinutesParked())
        {
            issue(car);
            return Boolean.TRUE;
        }
        else
        {
            return Boolean.FALSE;
        }


    }

    public void issue(ParkedCar car)
    {
        ParkingTicket ticket = new ParkingTicket(this, car);
        car.giveTicket(ticket);
    }
}

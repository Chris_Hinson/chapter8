//Main to demonstrate Area class for program 1 of chapter 8

import java.util.Scanner;

public class prg1
{
    public static void main(String args[])
    {

        Scanner k = new Scanner(System.in);

        System.out.println("Area of a circle");
        System.out.println("Please enter the radius");
        double radius = k.nextDouble();
        System.out.println("Area = " +Area.shapeArea(radius));

        System.out.println("Area of a rectangle");
        System.out.println("Please enter the length");
        double length = k.nextDouble();
        System.out.println("Please enter the width");
        double width = k.nextDouble();
        System.out.println("Area = " + Area.shapeArea(width,length));

        System.out.println("Area of a cylinder");
        System.out.println("Please enter the radius");
        float cylRadius = k.nextFloat();
        System.out.println("Please enter the height");
        double height = k.nextDouble();
        System.out.println("Area = " + Area.shapeArea(cylRadius,height));



    }
}
